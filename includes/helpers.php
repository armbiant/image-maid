<?php


////
//  Get distinct values of a meta key by post type
////
if(!function_exists('get_distinct_meta_values')){
    function get_distinct_meta_values( $key = '', $type = 'post', $status = 'publish', $wp_query = null ) {

        global $wpdb;

        if( empty( $key ) )
            return;

        $sql_query = "SELECT DISTINCT pm.meta_value FROM {$wpdb->postmeta} pm
            LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
            WHERE pm.meta_key = '%s' 
            AND p.post_status = '%s' 
            AND p.post_type = '%s'";
                    
        if(isset($wp_query)){
            $post_ids = wp_list_pluck( $wp_query->posts, 'ID' );
            if(count($post_ids) > 0){
                $sql_query .= " AND p.ID IN (" . implode(',', $post_ids) . ")";
            }
        }

        $r = $wpdb->get_col( $wpdb->prepare( $sql_query, $key, $status, $type ) );

        return $r;
    }
}


if(!function_exists('slugify')){
    function slugify($value){
        return strtolower(preg_replace('/[^a-zA-Z0-9]/', '-', trim($value)));
    }
}

if(!function_exists('dd')){
    function dd(){
        $args = func_get_args();
        foreach($args as $arg){
            var_dump($arg);
        }
        exit;
    }
};
