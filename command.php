<?php

require_once 'classes/ImageMaidClass.php';
require_once 'includes/helpers.php';

if ( ! class_exists( 'WP_CLI' ) ) {
	return;
}

/**
 * Hire the maid to clean up the images!
 *
 * @when before_wp_load
 */


WP_CLI::add_command( 'image-maid', 'ImageMaid' );


