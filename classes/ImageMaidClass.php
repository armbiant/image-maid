<?php

require_once 'ImageResizeClass.php';

//great article on image optimization https://www.smashingmagazine.com/2015/06/efficient-image-resizing-with-imagemagick/

class ImageMaid{

    public static $default_command_options = array(
        'return'     => true,   // Return 'STDOUT'; use 'all' for full object.
        'parse'      => 'json', // Parse captured STDOUT to JSON array.
        'launch'     => false,  // Reuse the current process.
        'exit_error' => false,   // Halt script execution on error.
    );
    
    /**
     * Resize all images greater than a supplied width and/or height.  Maintains aspect ratio
     *
     * ## OPTIONS
     *
     * <width>
     * : maximum width in px to trigger resize
     *
     * <height>
     * : maximum height in px to trigger resize
     *
     * [--optimize[=<optimization_level>]]
     * : Optimze quality of the image
     * ---
     * default: 95
     * ---
     * 
     * [--strip-meta[=<strip-meta>]]
     * : Strip Meta from images, maintain ICC
     * ---
     * default: true
     * options:
     *   - true
     *   - false
     * ---
     * 
     * [--png2jpg[=<png2jpg>]]
     * : Convert .png images to .jpg, only if no transparency detected
     * ---
     * default: true
     * options:
     *   - true
     *   - false
     * ---
     * 
     * [--regenerate-subsizes[=<regnerate-subsizes>]]
     * : delete all subsizes then regenerate them
     * ---
     * default: true
     * options:
     *   - true
     *   - false
     * ---
     *
     * [--start-page[=<start-page>]]
     * : Optimze quality of the image
     * ---
     * default: 1
     * ---
     * 
     * ## EXAMPLES
     *
     *     wp image-maid resize 300 300
     *     wp image-maid resize 300 300 --optimize=93
     *     wp image-maid resize 300 300 --optimize=93 --stripmeta
     *     wp image-maid resize 300 300 --optimize=93 --stripmeta --png2jpg
     *     wp image-maid resize 300 300 --optimize=93 --stripmeta --png2jpg --regeneratesubsizes
     *
     * @when after_wp_load
     */
    
    public function resize($args, $assoc_args){
        
        $width = intval($args[0]);
        $height = intval($args[1]);
        
        $optimization_level = isset($assoc_args['optimize']) ? intval($assoc_args['optimize']) : null;
        if(!is_null($optimization_level) && ($optimization_level < 1 || $optimization_level > 99)){
            WP_CLI::log("Invalid Optimization Level, no optimizations will take place");
            $optimization_level = null;
        }
        
        $strip_meta = isset($assoc_args['strip-meta']) ? filter_var($assoc_args['strip-meta'], FILTER_VALIDATE_BOOLEAN) : null;
        $png2jpg = isset($assoc_args['png2jpg']) ? filter_var($assoc_args['png2jpg'], FILTER_VALIDATE_BOOLEAN) : null;
        $regenerate_subsizes = isset($assoc_args['regenerate-subsizes']) ? filter_var($assoc_args['regenerate-subsizes'], FILTER_VALIDATE_BOOLEAN) : null;
        
        //run this paged since it kills mysql queries due to long ass queries
        //quickly count how many attachments we have
        $num_attachments = WP_CLI::runcommand('post list --post_type=attachment --format=count', Self::$default_command_options);
        
        //figure out how many pages.
        $num_per_page = 20;
        $num_pages = (int) ceil($num_attachments / $num_per_page);

        $start_page = isset($assoc_args['start-page']) ? intval($assoc_args['start-page']) : 1;
        if($start_page > $num_pages){
            WP_CLI::log("Invalid start page, aborting.");
            exit;
        }
        
        //loop through pages
        for($page_i = $start_page; $page_i <= $num_pages; $page_i++){            
            
            $attachments = WP_CLI::runcommand("post list --post_type=attachment --format=json --fields=ID,post_mime_type --posts_per_page={$num_per_page} --paged={$page_i}", Self::$default_command_options);
            WP_CLI::log("---------- Processing Page $page_i ----------");
            
            //loop the actual attachments (media)
            foreach($attachments as $attachment){     
                WP_CLI::log("\n");
                 //make sure we don't have the imagesize filter still set so that we delete all existing image sizes
                 //that aren't needed.  would only need this if one of the below functions threw an error half way
                 //between udpating attachment meta
                 if(has_filter('intermediate_image_sizes_advanced', '\ImageResize::removeDefaultImageSizes')){
                    WP_CLI::log("REMOVING FILTER TO REMOVE DEFAULT IMAGE SIZES");
                    remove_filter( 'intermediate_image_sizes_advanced', '\ImageResize::removeDefaultImageSizes');
                }
                try {
                    
                    $image = new ImageResize($attachment['ID'], $attachment['post_mime_type']);                
                    //$image->resize($width, $height);
                    //$image->resizeWpApproach($width, $height);
                    $image->resizeMattApproach($width, $height);
                    if(!is_null($strip_meta)){
                        $image->stripMeta();
                    }
                    if(!is_null($png2jpg)){
                        $image->convertPng2Jpg();
                        //might want to resize again if that helps anything....  or optimize                        
                    }
                    if(!is_null($optimization_level)){
                        $image->optimize($optimization_level);
                    }
                    if(!is_null($regenerate_subsizes)){
                        $image->regenerateSubsizes();
                    }                    
                } catch (Exception $e) {
                    WP_CLI::log("COULDN'T PROCESS IMAGE {$attachment['ID']}: {$e->getMessage()}");
                    //carry on wayward soldier;   
                }
            }
        }
        
        WP_CLI::success("You did it!");
    }
    
    
}